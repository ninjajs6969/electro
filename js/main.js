$(document).ready(function(){
  $('.carusel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 2,
    dots: true,
    mobileFirst: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
                
            },
            {
                breakpoint: 600,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
              
            },
            {
                breakpoint: 320,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
            }
            
          },
            {
              breakpoint: 1400,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 2
                }
                
            }
        ]
  });
});


//burger

$(document).ready(function(){
  $("#burger-container").on('click', function(){
      $(this).toggleClass("open");
  });

  const navToggle = $("#burger-container");
  const nav = $("#nav")


  navToggle.on("click", function(event){
    event.preventDefault();
    nav.toggleClass("show");
  });

});